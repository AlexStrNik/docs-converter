use chrono::{prelude::*, Duration};
use reqwest::header::{AUTHORIZATION, LOCATION, CONTENT_TYPE};
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt;
use std::path::Path;

const CLIENT_ID: &str = env!("CLIENT_ID");
const CLIENT_SECRET: &str = env!("CLIENT_SECRET");
const REDIRECT_URI: &str = env!("REDIRECT_URI");

#[derive(Debug)]
struct ApiError {
    details: String,
}

impl ApiError {
    fn new(msg: String) -> Self {
        Self { details: msg }
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for ApiError {
    fn description(&self) -> &str {
        &self.details
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
enum AuthResponse<T> {
    Ok(T),
    Err {
        error: String,
        error_description: String,
    },
}

#[derive(Deserialize)]
#[serde(untagged)]
enum ApiResponse<T> {
    Ok(T),
    Err { error: serde_json::Value },
}

#[derive(Deserialize, Debug)]
struct TokensPair {
    access_token: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    refresh_token: Option<String>,
    expires_in: i64,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Auth {
    pub access_token: String,
    pub refresh_token: String,
    pub expires_in: DateTime<Utc>,
}

#[derive(Serialize, Debug, Deserialize)]
pub struct Permissions {
    role: String,
    #[serde(rename = "type")]
    kind: String,
}

impl Permissions {
    fn default() -> Self {
        Self {
            role: "reader".to_string(),
            kind: "anyone".to_string(),
        }
    }
}

impl Auth {
    pub async fn sign_in(code: String) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let client = reqwest::Client::new();
        let params = [
            ("code", code),
            ("client_id", CLIENT_ID.to_string()),
            ("client_secret", CLIENT_SECRET.to_string()),
            ("grant_type", "authorization_code".to_string()),
            ("redirect_uri", REDIRECT_URI.to_string()),
        ];

        let resp: AuthResponse<TokensPair> = client
            .post("https://oauth2.googleapis.com/token")
            .form(&params)
            .send()
            .await?
            .json()
            .await?;

        match resp {
            AuthResponse::Ok(pair) => {
                let refresh_token = if let Some(refresh_token) = pair.refresh_token {
                    refresh_token
                } else {
                    return Err(ApiError::new(format!("ApiError: refresh_token is missing")).into());
                };

                Ok(Auth {
                    access_token: pair.access_token,
                    refresh_token,
                    expires_in: Utc::now() + Duration::seconds(pair.expires_in),
                })
            }
            AuthResponse::Err {
                error,
                error_description,
            } => Err(ApiError::new(format!("ApiError: {}, {}", error, error_description)).into()),
        }
    }

    pub async fn refresh(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        let client = reqwest::Client::new();
        let params = [
            ("refresh_token", self.refresh_token.clone()),
            ("client_id", CLIENT_ID.to_string()),
            ("client_secret", CLIENT_SECRET.to_string()),
            ("grant_type", "refresh_token".to_string()),
            ("redirect_uri", REDIRECT_URI.to_string()),
        ];

        let resp: AuthResponse<TokensPair> = client
            .post("https://oauth2.googleapis.com/token")
            .form(&params)
            .send()
            .await?
            .json()
            .await?;

        match resp {
            AuthResponse::Ok(pair) => {
                self.access_token = pair.access_token;
                self.expires_in = Utc::now() + Duration::seconds(pair.expires_in);
            }
            AuthResponse::Err {
                error,
                error_description,
            } => return Err(ApiError::new(format!("ApiError: {}, {}", error, error_description)).into()),
        };

        Ok(())
    }
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct DocMeta {
    pub name: String,
    pub mime_type: String,
}

impl DocMeta {
    pub fn from_name(file_name: &String) -> Option<Self> {
        let name = Path::new(file_name).file_stem().and_then(|x| x.to_str())?;
        let mime_type = get_docs_mime(file_name)?;

        Some(Self {
            name: name.to_string(),
            mime_type,
        })
    }
}

#[derive(Deserialize, Debug)]
pub struct FileID {
    id: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct FileUrl {
    web_view_link: String,
}

pub fn auth_url(chat_id: String) -> String {
    format!(
        "https://accounts.google.com/o/oauth2/v2/auth?\
         scope=https%3A//www.googleapis.com/auth/drive.file&\
         access_type=offline&\
         include_granted_scopes=true&\
         response_type=code&\
         redirect_uri={redirect_uri}&\
         prompt=consent&\
         state={chat_id}&\
         client_id={client_id}",
        redirect_uri = REDIRECT_URI,
        chat_id = chat_id,
        client_id = CLIENT_ID
    )
}

fn get_docs_mime(name: &String) -> Option<String> {
    let extension = Path::new(name).extension().and_then(|x| x.to_str())?;

    match extension {
        "docx" | "doc" | "odt" | "rtf" => Some("application/vnd.google-apps.document".to_string()),
        "xlsx" | "xls" | "csv" | "tsv" | "ods" => {
            Some("application/vnd.google-apps.spreadsheet".to_string())
        }
        "ppt" | "pptx" | "odp" => Some("application/vnd.google-apps.presentation".to_string()),
        _ => None,
    }
}

pub fn get_file_mime(name: &String) -> Option<&str> {
    let extension = Path::new(name).extension().and_then(|x| x.to_str())?;

    match extension {
        "docx" => Some("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
        "doc" => Some("application/msword"),
        "odt" => Some("application/vnd.oasis.opendocument.text"),
        "rtf" => Some("application/rtf"),
        "xlsx" => Some("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
        "xls" => Some("application/vnd.ms-excel"),
        "csv" => Some("text/csv"),
        "tsv" => Some("text/tab-separated-values"),
        "ods" => Some("application/vnd.oasis.opendocument.spreadsheet"),
        "ppt" => Some("application/vnd.ms-powerpoint"),
        "pptx" => Some("application/vnd.openxmlformats-officedocument.presentationml.presentation"),
        "odp" => Some("application/vnd.oasis.opendocument.presentation"),
        _ => None
    }
}

pub async fn get_file_url(auth: &mut Auth, id: String) -> Result<String, Box<dyn Error + Send + Sync>> {
    check_auth(auth).await?;

    let client = reqwest::Client::new();
    let resp: ApiResponse<FileUrl> = client
        .get(
            format!(
                "https://www.googleapis.com/drive/v3/files/{}?fields=webViewLink",
                id
            )
            .as_str(),
        )
        .header(AUTHORIZATION, format!("Bearer {}", auth.access_token))
        .send()
        .await?
        .json()
        .await?;

    match resp {
        ApiResponse::Ok(file) => Ok(file.web_view_link),
        ApiResponse::Err { error } => Err(ApiError::new(error.to_string()).into()),
    }
}

async fn check_auth(auth: &mut Auth) -> Result<(), Box<dyn Error + Send + Sync>> {
    if Utc::now() > auth.expires_in {
        auth.refresh().await?;
    }

    Ok(())
}

pub async fn upload_document(
    auth: &mut Auth,
    meta: DocMeta,
    mime: &str,
    bytes: Vec<u8>,
) -> Result<(String, String), Box<dyn Error + Send + Sync>> {
    check_auth(auth).await?;

    let client = reqwest::Client::new();
    let resp = client
        .post("https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable")
        .header(AUTHORIZATION, format!("Bearer {}", auth.access_token))
        .header("X-Upload-Content-Type", mime)
        .json(&meta)
        .send()
        .await?;

    let location = resp.headers().get(LOCATION).cloned();

    let location = if let Some(location) = location {
        location
    } else {
        let resp: ApiResponse<()> = resp.json().await?;
        match resp {
            ApiResponse::Ok(_) => unreachable!(),
            ApiResponse::Err { error } => return Err(ApiError::new(error.to_string()).into()),
        };
    };

    let resp: ApiResponse<FileID> = client
        .put(location.to_str().unwrap())
        .body(bytes)
        .header(CONTENT_TYPE, mime)
        .send()
        .await?
        .json()
        .await?;

    match resp {
        ApiResponse::Ok(file) => {
            let file_id = file.id.clone();
            get_file_url(auth, file.id).await.map(|url| (url, file_id))
        }
        ApiResponse::Err { error } => Err(ApiError::new(error.to_string()).into()),
    }
}

pub async fn share_document(
    auth: &mut Auth,
    id: String,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    check_auth(auth).await?;

    let client = reqwest::Client::new();
    let resp: ApiResponse<Permissions> = client.post(format!(
        "https://www.googleapis.com/drive/v3/files/{}/permissions",
        id
    ).as_str())
    .header(AUTHORIZATION, format!("Bearer {}", auth.access_token))
    .json(&Permissions::default())
    .send()
    .await?
    .json()
    .await?;

    match resp {
        ApiResponse::Ok(_) => Ok(()),
        ApiResponse::Err { error } => Err(ApiError::new(error.to_string()).into()),
    }
}