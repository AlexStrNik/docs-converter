mod api;

use api::{Auth, DocMeta, auth_url, get_file_mime, get_file_url, share_document, upload_document};
use serde::Deserialize;
use std::{convert::Infallible, net::SocketAddr};
use tbot::{
    markup::{bold, markdown_v2},
    prelude::*,
    state::Chats,
    types::{
        chat::{self, Action::Typing},
        keyboard::inline::{Button, ButtonKind, Keyboard},
        parameters::Text as ParseMode,
    },
};
use tera::{Context, Tera};
use tokio::{select, sync::Mutex};
use warp::Filter;
use std::fs::{read_to_string, write};

#[derive(Deserialize)]
struct AuthParams {
    code: Option<String>,
    error: Option<String>,
    state: Option<String>,
}

fn render_page(message: &str) -> impl warp::Reply {
    let mut context = Context::new();
    context.insert("message", message);

    warp::reply::html(Tera::one_off(include_str!("template.html"), &context, false).unwrap())
}

#[tokio::main]
async fn main() {
    let addr = SocketAddr::from(([127, 0, 0, 1], env!("SERVER_PORT").parse().unwrap()));

    let bot = tbot::from_env!("BOT_TOKEN");
    let bot_clone = bot.clone();

    let saved_state: Chats<Auth> = match read_to_string("saved_state.json") {
        Ok(saved_state) => serde_json::from_str(saved_state.as_str()).unwrap(),
        Err(_) => Chats::new()
    };

    let mut bot = bot.stateful_event_loop::<Mutex<Chats<Auth>>>(Mutex::new(saved_state));

    bot.fetch_username().await.unwrap();

    bot.start(|context, state| async move {
        if state.lock().await.has(&*context) {
            //TODO: send help message
        } else {
            context
                .send_message_in_reply("Please sign in to start using the bot")
                .reply_markup(Keyboard::new(&[&[Button::new(
                    "Sign In",
                    ButtonKind::Url(&auth_url(context.chat.id.to_string())),
                )]]))
                .call()
                .await
                .unwrap();
            return;
        }
    });

    bot.document(|context, state| async move {
        let mut state = state.lock().await;

        let auth = if let Some(auth) = state.get_mut(&*context) {
            auth
        } else {
            context
                .send_message_in_reply("Please sign in to start using the bot")
                .reply_markup(Keyboard::new(&[&[Button::new(
                    "Sign In",
                    ButtonKind::Url(&auth_url(context.chat.id.to_string())),
                )]]))
                .call()
                .await
                .unwrap();
            return;
        };

        let file_name = if let Some(file_name) = &context.document.file_name {
            file_name
        } else {
            //TODO: send message about supported formats
            return;
        };

        let meta_data = if let Some(meta_data) = DocMeta::from_name(file_name) {
            meta_data
        } else {
            //TODO: send message about supported formats
            return;
        };

        let mime = if let Some(mime) = get_file_mime(file_name) {
            mime
        } else {
            return;
        };

        let call_result = context
            .bot
            .get_file(context.document.file_id.as_ref())
            .call()
            .await;
        let file = match call_result {
            Ok(file) => file,
            Err(err) => {
                dbg!(err);
                return;
            }
        };

        let call_result = context.bot.download_file(&file).await;
        let bytes = match call_result {
            Ok(bytes) => bytes,
            Err(err) => {
                dbg!(err);
                return;
            }
        };

        let upload_document = async {
            match upload_document(auth, meta_data, mime, bytes).await {
                Ok((url, id)) => {
                    context
                        .send_message_in_reply(
                            "Yay! Uploading complete!. Open document by clicking button",
                        )
                        .reply_markup(Keyboard::new(&[
                            &[Button::new("Open Document", ButtonKind::Url(&url))],
                            &[Button::new(
                                "Share Document",
                                ButtonKind::CallbackData(id.as_str()),
                            )],
                        ]))
                        .call()
                        .await
                        .unwrap();
                }
                Err(e) => {
                    let err_string = e.to_string();

                    let message = markdown_v2((
                        "Oops. Something went wrong while uploading document. Full error: ",
                        bold(err_string),
                    ))
                    .to_string();

                    let message = ParseMode::with_markdown_v2(&message);
                    context.send_message_in_reply(message).call().await.unwrap();
                }
            }
        };

        select! {
            _ = upload_document => (),
            _ = context.send_chat_action_in_loop(Typing) => (),
        }
    });

    bot.message_data_callback(|context, state| async move {
        let mut state = state.lock().await;

        let auth = if let Some(auth) = state.get_mut(&*context) {
            auth
        } else {
            context
                .send_message_in_reply("Please sign in to start using the bot")
                .reply_markup(Keyboard::new(&[&[Button::new(
                    "Sign In",
                    ButtonKind::Url(&auth_url(context.message.chat.id.to_string())),
                )]]))
                .call()
                .await
                .unwrap();
            return;
        };

        let url = get_file_url(auth, context.data.clone()).await;
        match share_document(auth, context.data.clone()).await.and(url) {
            Ok(url) => {
                let message = markdown_v2((
                    "Done! Now you can send this link to anyone you want to share with. \n\n",
                    url
                )).to_string();

                let message = ParseMode::with_markdown_v2(&message);
                context.send_message_in_reply(message).call().await.unwrap();
            }
            Err(e) => {
                let err_string = e.to_string();

                let message = markdown_v2((
                    "Oops. Something went wrong while uploading document. Full error: ",
                    bold(err_string),
                ))
                .to_string();

                let message = ParseMode::with_markdown_v2(&message);
                context.send_message_in_reply(message).call().await.unwrap();
            }
        }
    });

    let state = bot.get_state();

    let auth = warp::get()
        .and(warp::query())
        .and_then(move |params: AuthParams| {
            let state = state.clone();
            let bot = bot_clone.clone();

            async move {
                if let Some(error) = params.error {
                    return Ok::<_, Infallible>(render_page(error.as_str()));
                }

                let code = if let Some(code) = params.code {
                    code
                } else {
                    return Ok(render_page("query parameter `code` is misisng"));
                };

                let chat_id = if let Some(chat_id) = params.state {
                    chat_id
                } else {
                    return Ok(render_page("query parameter `state` is misisng"));
                };

                let chat_id: chat::Id = chat_id.parse::<i64>().unwrap().into();

                let tokens_pair = Auth::sign_in(code.to_owned()).await;

                let tokens_pair = match tokens_pair {
                    Err(e) => {
                        let err_string = e.to_string();

                        let message = markdown_v2((
                        "Oops. Something went wrong while bot trying to authorize. Full error: ",
                        bold(err_string),
                    ))
                    .to_string();
                        let message = ParseMode::with_markdown_v2(&message);

                        bot.send_message(chat_id, message).call().await.unwrap();

                        return Ok(render_page("Something went wrong"));
                    }
                    Ok(pair) => pair,
                };

                state
                    .lock()
                    .await
                    .entry_by_id(chat_id)
                    .and_modify(|pair| *pair = tokens_pair.clone())
                    .or_insert(tokens_pair);

                bot.send_message(chat_id, "Yay! Bot successfully authorized!")
                    .call()
                    .await
                    .unwrap();

                Ok(render_page("Now you can close this page"))
            }
        });

    let state = bot.get_state();

    tokio::spawn(async move {
        bot.polling().start().await.unwrap();
    });

    ctrlc::set_handler(move || {
        let state_to_save = serde_json::to_string(&state.try_lock().unwrap().to_owned()).unwrap();
        write("saved_state.json", state_to_save).unwrap();
        std::process::exit(0);
    }).unwrap();

    warp::serve(auth).run(addr).await;
}
